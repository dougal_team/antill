from django.contrib import admin

# Register your models here.
import leaflet.admin

from anthill.models import City, Subscriber, Place, Comment, Feedback, Image

admin.site.site_header = "SpaceTime - адміністрування сайту"


class CityAdmin(leaflet.admin.LeafletGeoAdmin):
    # fields to show in admin listview
    readonly_fields = ('id',)
    list_display = ('name', 'geometry')


class PlaceAdmin(leaflet.admin.LeafletGeoAdmin):
    readonly_fields = ('id',)
    list_display = ('name', 'approved', 'geometry')


admin.site.register(City, CityAdmin)
admin.site.register(Subscriber)
admin.site.register(Place, PlaceAdmin)
admin.site.register(Comment)
admin.site.register(Feedback)
admin.site.register(Image)



