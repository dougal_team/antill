from django.apps import AppConfig


class AnthillConfig(AppConfig):
    name = 'anthill'
