from django import forms
from django.forms import ModelForm
from anthill.models import Comment

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = ['subject', 'text', 'rating']