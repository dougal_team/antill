# Generated by Django 2.2 on 2019-07-11 19:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('anthill', '0005_auto_20190711_1637'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='place',
            options={'ordering': ('name',), 'verbose_name_plural': 'places'},
        ),
        migrations.AddField(
            model_name='comment',
            name='rating',
            field=models.IntegerField(default=1),
        ),
        migrations.AddField(
            model_name='place',
            name='rating',
            field=models.IntegerField(default=1),
        ),
        migrations.AlterField(
            model_name='place',
            name='header_img',
            field=models.ImageField(null=True, upload_to='place/headers'),
        ),
    ]
