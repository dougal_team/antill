# Generated by Django 2.2 on 2019-07-26 20:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('anthill', '0010_auto_20190722_1909'),
    ]

    operations = [
        migrations.AddField(
            model_name='place',
            name='google_id',
            field=models.CharField(blank=True, max_length=255),
        ),
        migrations.AddField(
            model_name='place',
            name='total_ratings',
            field=models.IntegerField(default=-1),
        ),
        migrations.AlterField(
            model_name='place',
            name='address',
            field=models.CharField(max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='place',
            name='price',
            field=models.IntegerField(default=-1),
        ),
        migrations.AlterField(
            model_name='place',
            name='type',
            field=models.CharField(choices=[('AC', 'Активний відпочинок'), ('EC', 'Екотуризм'), ('NT', 'Природа та парки'), ('AR', 'Музеї, архітектура, культура'), ('OT', 'Інше')], default='NT', max_length=2),
        ),
    ]
