from django.contrib.auth.models import User
from django.db import models
from django.contrib.gis.db import models as geomodels
from imagekit.models import ImageSpecField
from tinymce.models import HTMLField
from imagekit.processors import ResizeToFill


class Subscriber(models.Model):
    email = models.EmailField(blank=False, unique=True)
    time = models.DateTimeField(auto_now_add=True, blank=True)

    def __str__(self):
        return self.email


class Feedback(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField()
    subject = models.CharField(max_length=100)
    text = models.TextField()

    def __str__(self):
        return self.subject


class City(models.Model):
    name = models.CharField(max_length=150, blank=False, unique=True)
    description = models.TextField(null=True)
    geometry = geomodels.PointField()
    main_image = models.ImageField(upload_to="city_img", null=True)
    city_photo = ImageSpecField(source='main_image',
                                processors=[ResizeToFill(370, 420)],
                                format='JPEG',
                                options={'quality': 60})

    class Meta:
        ordering = ('name',)
        verbose_name_plural = 'cities'

    def __str__(self):
        return self.name


class Place(models.Model):
    ACTIVE = 'AC'
    ECO = 'EC'
    NATURE = 'NT'
    ARCHITECTURE = 'AR'
    OTHER = 'OT'

    TYPE_OF_PLACE_CHOICE = (
        (ACTIVE, 'Активний відпочинок'),
        (ECO, 'Екотуризм'),
        (NATURE, 'Природа та парки'),
        (ARCHITECTURE, 'Музеї, архітектура, культура'),
        (OTHER, 'Інше')
    )

    name = models.CharField(max_length=150, blank=False)
    geometry = geomodels.PointField(null=True)
    type = models.CharField(max_length=2, choices=TYPE_OF_PLACE_CHOICE, default=NATURE)

    approved = models.BooleanField(default=False)

    header_img = models.ImageField(upload_to="place/headers", null=True)

    place_header = ImageSpecField(source='header_img',
                                processors=[ResizeToFill(1920, 803)],
                                format='JPEG',
                                options={'quality': 80})

    place_logo = ImageSpecField(source='header_img',
                                processors=[ResizeToFill(290, 240)],
                                format='JPEG',
                                options={'quality': 80})

    address = models.CharField(max_length=255, null=True)
    content = HTMLField()

    price = models.IntegerField(default=-1)
    phone = models.CharField(max_length=255)
    site = models.URLField(null=True, blank=True)
    facebook_link = models.URLField(null=True, blank=True)
    email = models.EmailField(blank=True, null=True)

    city = models.ForeignKey(City, on_delete=models.CASCADE)

    # Features
    free = models.BooleanField(default=False)
    for_kids = models.BooleanField(default=False)
    inclusive = models.BooleanField(default=False)
    extreme = models.BooleanField(default=False)
    romantic = models.BooleanField(default=False)
    family = models.BooleanField(default=False)


    visit_count = models.IntegerField(default=0)
    user_likes = models.ManyToManyField(User)
    rating = models.IntegerField(default=-1)
    total_ratings = models.IntegerField(default=-1)
    google_id = models.CharField(max_length=255, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('name', )
        verbose_name_plural = 'places'
        unique_together = [['name', 'geometry']]

    def natural_key(self):
        return (self.name, self.geometry.x, self.geometry.y)


class Comment(models.Model):
    subject = models.CharField(max_length=100, blank=False)
    text = models.TextField()
    place = models.ForeignKey(Place, on_delete=models.CASCADE)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    time = models.DateTimeField(auto_now_add=True, blank=True)
    rating = models.IntegerField(default=0)
    likes = models.IntegerField(default=0)

    def __str__(self):
        return self.subject


class Image(models.Model):
    title = models.CharField(max_length=250)
    data = models.ImageField(upload_to="coworkings/albums")
    data_thumbnail = ImageSpecField(source='data',
                                    processors=[ResizeToFill(250, 180)],
                                    format='JPEG',
                                    options={'quality': 60})
    date_uploaded = models.DateTimeField(auto_now_add=True)
    place = models.ForeignKey(Place, on_delete=models.CASCADE)

    def __str__(self):
        return self.title




