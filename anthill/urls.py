from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path, include

from olivet import settings

from . import views

app_name = 'anthill'

urlpatterns = [
    path('', views.index, name='index'),
    path('logout/', views.logout_view, name='logout'),
    path('login/', views.signin, name='login'),
    path('register/', views.register, name='register'),
    path('accounts/login/', views.signin, name="login"),
    path('cabinet/', views.cabinet, name='cabinet'),
    path('partner/', views.partner, name='partner'),

    path('subscribe/', views.subscribe, name='subscribe'),
    path('citymap/', views.citymap, name='citymap'),
    path('places/', views.places, name='places'),
    path('contact/', views.contact, name='contact'),
    path('place/', views.place, name='place'),
    path('city_location/', views.city_location, name='city_location'),
    path('booking/', views.booking, name='booking'),

    path('not_implemented/', views.not_implemented, name='not_implemented/'),
    path('like/', views.like, name='like'),
    path('like_comment/', views.like_comment, name='like_comment'),
    path('thanks/', views.comment_thanks, name='comment_thanks'),

    #api
    path('place_import/', views.place_import, name='place_import'),

    url(r'^tinymce/', include('tinymce.urls'))
]


urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
