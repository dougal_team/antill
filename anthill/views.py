import json

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.gis.geos import Point
from django.contrib.gis.measure import D
from django.core import serializers
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.db.models import Count

import logging

# Create your views here.
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from django.contrib.admin.views.decorators import staff_member_required

from anthill.models import City, Subscriber, Place, Feedback, Comment, Image
from anthill.forms import CommentForm

logger = logging.getLogger(__name__)


# START Infra pages


def index(request):
    logger.debug('index() - page requested')
    return render(request, 'anthill/index.html', get_statistics())


def contact(request):
    logger.debug('contact() - page requested')
    if request.POST:
        name = request.POST.get("name")
        email = request.POST.get("email")
        subject = request.POST.get("subject")
        comment = request.POST.get("comment")
        logger.info("contact() - saving %s, %s from %s", name, email, subject)
        feedback = Feedback(name=name, email=email, subject=subject, text=comment)
        Feedback.save(feedback)
        return render(request, 'anthill/contact.html', {"message": "Дякуємо за відгук!"})
    else:
        return render(request, 'anthill/contact.html')


def subscribe(request):
    logger.debug('subscribe() - page requested')
    if request.method == 'POST':
        mail = request.POST['email-sub']
        s = Subscriber(email=mail)
        try:
            s.save()
        except Exception as exc:
            logger.warning("subscribe() - can't save: %s", exc)

    return render(request, 'anthill/subscribe.html')


@csrf_exempt
@login_required
def like(request):
    try:
        place_id = request.POST.get('place_id')

        place = Place.objects.get(pk=place_id)
        place.user_likes.add(request.user)
        return HttpResponse(200)

    except Exception as exc:
        logger.error('like() - error: ', exc)
        return HttpResponse(500)


@csrf_exempt
def like_comment(request):
    comment_id = request.POST.get('comment_id')
    comment = Comment.objects.get(pk=comment_id)
    comment.likes += 1
    comment.save()
    return HttpResponse(200)


def signin(request):
    logger.debug('signin() - page requested')
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        next_page = request.POST.get('next')

        user = authenticate(request, username=username, password=password)

        if user is not None:
            logger.info("signin() - user: %s", username)
            login(request, user)

            if request.POST.get('rememb-check'):
                request.session.set_expiry(0)

            if next_page != "None":
                return redirect(next_page)

            return redirect('/')
        else:
            context = {'error_message': 'Помилка при вході! Перевірте дані'}
            return render(request, 'anthill/login.html', context)
    else:
        context = {"next": request.GET.get('next')}
        return render(request, 'anthill/login.html', context)


def register(request):
    logger.debug('register() - page requested')
    if request.method == 'POST':
        email = request.POST.get('email')
        fname = request.POST.get('firstname')
        lname = request.POST.get('lastname')

        password = request.POST.get('password')
        new_user = get_user_model().objects.create_user(username=email,
                                                        password=password,
                                                        email=email)
        new_user.is_active = True
        new_user.first_name = fname
        new_user.last_name = lname
        new_user.save()

        request.session['user_name'] = email
        request.session['user_password'] = password
        request.session['user_email'] = email

        logger.info("register() - user created: %s, %s", email, fname)
        return redirect('/')

    else:
        return render(request, 'anthill/register.html', {})


@login_required
def cabinet(request):
    return render(request, 'anthill/cabinet.html', {})

@login_required
def partner(request):
    return render(request, 'anthill/partner_cabinet.html', {})

@login_required
def booking(request):
    return render(request, 'anthill/booking.html', {})


def logout_view(request):
    logger.debug('logout() - page requested')
    logout(request)
    return redirect('/')

# EVENTS pages


def not_implemented(request):
    return render(request, 'anthill/notimplemented.html')

# END Infra pages


# START places pages
@require_http_methods(["GET"])
def citymap(request):
    logger.debug('citymap() - page requested')
    city_val = request.GET.get("city")
    type_val = request.GET.get("category")

    all_cities = City.objects.all()
    return render(request, 'anthill/citymap.html', {"cities": all_cities, "city_val": city_val, "category": type_val })


def city_location(request):
    city_id = request.GET.get("id")
    city = City.objects.get(pk=city_id)

    context = {
        'city': city.name.encode("utf-8").decode("utf-8"),
        'x': city.geometry.x,
        'y': city.geometry.y
    }

    return JsonResponse(context)


@require_http_methods(["GET"])
def places(request):
    """
    Returns list of objects according to filters
    :param request:
    :return:
    """
    logger.debug('places() - page requested')
    context = filter_places(request)
    return render(request, 'anthill/places.html', context)


def place(request):
    if request.method == 'POST':
        save_comment(request)
        return HttpResponseRedirect("/thanks", {})
    else:
        place_id = request.GET.get("id")

    context = {}
    place = Place.objects.get(pk=place_id)
    context['place'] = place
    place.visit_count += 1
    place.save()

    if Image.objects.filter(place__id=place_id).count() > 0:
        images = Image.objects.filter(place__id=place_id)
        context['images'] = images

    if Place.objects.filter(user_likes__id = request.user.id).count() > 0:
        print('liked')
        context['liked'] = True

    context['comments'] = Comment.objects.filter(place__id=place_id)

    return render(request, 'anthill/place.html', context)


def comment_thanks(request):
    return render(request, 'anthill/comment_thanks.html', {})


# place scrapper api
@staff_member_required
def place_import(request):
    if request.method == "POST":
        json_text = request.FILES['file'].read().decode("utf-8")
        json_data = json.loads(json_text)
        logger.info('place_import() - importing %s', json_data)
        try:
            save_json_places(json_data)
            return redirect('/')
        except Exception as e:
            logger.error('place_import() - error: %s', e)
            return HttpResponse(500)

    if request.method == "GET":
        return render(request, 'anthill/place_import.html')



def save_comment(request):
    form = CommentForm(request.POST)
    place_id = request.POST.get('place_id')
    if form.is_valid():
        comment = form.save(commit=False)

        place = Place.objects.get(pk=place_id)

        comment.author = request.user
        comment.place = place
        comment.save()

    else:
        logging.error('saving wrong form ' + form.errors)
    return place_id


# END places pages
def filter_places(request):
    """
    Filtering places by user input
    :param request:
    :return:
    """
    city = request.GET.get("city")
    category = request.GET.get("category")
    query = {}

    if category != "all":
        query["type"] = category

    all_places = set_full_filter(city, query, request)

    places_json = serializers.serialize('json', all_places, fields=["geometry", "name", "id"])
    context = {"places": all_places, "places_json": places_json}

    return context


def set_full_filter(city, query, request):
    filter_values = ['free', 'for_kids', 'inclusive', 'extreme', 'family', 'romantic']

    for val in filter_values:
        if request.GET.get(val) == "true":
            query[val] = True

    query['approved'] = True

    if city == "closest":
        lat = float(request.GET.get("lat"))
        lon = float(request.GET.get("lon"))
        location_point = Point(lon, lat)
        current_city = City.objects.filter(geometry__distance_lte=(location_point, D(km=10)))
        if current_city:
            current_city = current_city[0]
            query['city__id'] = current_city.id

        all_places = Place.objects.filter(**query).annotate(number_of_feebacks=Count('comment'))

    else:
        query['city__id'] = city
        all_places = Place.objects.filter(**query).annotate(number_of_feebacks=Count('comment'))

    return all_places


def get_statistics():
    statistics = {
        'cities': City.objects.all().annotate(number_of_places=Count('place')),
        'locations_count': Place.objects.count(),
        'comments_count': Comment.objects.count(),
        'users_count': User.objects.count(),
        'places': Place.objects.filter(approved=True).annotate(number_of_feebacks=Count('comment'))[:10]
    }

    return statistics


def save_json_places(json_data):
    for place_json in json_data:

        place = Place()
        place.name = place_json['name']
        place.address = place_json['address']
        place.rating = place_json['rating']
        place.total_ratings = place_json['total_ratings']
        place.google_id = place_json['google_id']
        place.geometry = Point(place_json['lan'], place_json['lat'])
        place.type = place_json['types']

        city = City.objects.get(pk=place_json['city_id'])
        place.city = city

        place.save()
