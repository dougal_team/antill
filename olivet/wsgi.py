"""
WSGI config for olivet project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

import sys



sys.path.append(os.path.expanduser('~'))
sys.path.append(os.path.expanduser('~') + '/ROOT/')

#import runpy
#file_globals = runpy.run_path(conda_path)


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'olivet.settings')

application = get_wsgi_application()

