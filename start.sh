#!/bin/bash

# Start Gunicorn processes
echo Starting Gunicorn.
exec gunicorn olivet.wsgi:application \
    --bind 0.0.0.0:8000 \
    --workers 3 \
    --log-level=debug \
    --log-file=/srv/www/anthill/site.log
    --access--logfile=/srv/www/gunicorn-access.log